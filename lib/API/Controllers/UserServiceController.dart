import 'package:get_storage/get_storage.dart';
import 'package:power/API/Models/ServiceModel.dart';
import 'package:power/API/Models/UserServiceModel.dart';
import 'Controller.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';

class UserServiceController{


  static Future<List<UserServiceModel>> myServices()async{
    var response =
    await http.get(Uri.parse('${Controller.url}/service/my'), headers: {
      'Accept': 'application/json',
      'Authorization': 'Bearer ${GetStorage().read('token')}'
    });
    Map<String, dynamic> json = jsonDecode(response.body);

    List<UserServiceModel> models = [];

    for(var x in json['services'])
      models.add(UserServiceModel.fromJSON(x));

    return models;
  }

  static Future<List<ServiceModel>> allServices()async{
    var response =
    await http.get(Uri.parse('${Controller.url}/sub_service/city/${GetStorage().read('city_id')}'));
    Map<String, dynamic> json = jsonDecode(response.body);
    List<ServiceModel> models = [];

    for(var s in json['sub_services'])
      models.add(ServiceModel.fromJSON(s));

    return models;
  }

  static Future<String> byService(id)async{
    var response =
    await http.post(Uri.parse('${Controller.url}/service/by'),
    body: jsonEncode({
      'sub_service_id' : id
    }),
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ${GetStorage().read('token')}',
        'Content-Type': 'application/json'
      }
    );
    return "service added successfully";
  }

  static Future<ServiceModel> showService(id)async{
    var response =
    await http.get(Uri.parse('${Controller.url}/sub_service/show/$id'),);

    Map<String,dynamic> json = jsonDecode(response.body);
    print(json);
    return ServiceModel.fromJSON(json['sub_service']);
  }

  static Future<String> payService(id)async{
    var response =
    await http.put(Uri.parse('${Controller.url}/service/pay/$id'),
        headers: {
          'Accept': 'application/json',
          'Authorization': 'Bearer ${GetStorage().read('token')}',
          'Content-Type': 'application/json'
        }
    );
    return "service payed successfully";
  }

  static Future<String> payAllServices()async{
    var response =
    await http.put(Uri.parse('${Controller.url}/service/payAll'),
        headers: {
          'Accept': 'application/json',
          'Authorization': 'Bearer ${GetStorage().read('token')}',
          'Content-Type': 'application/json'
        }
    );
    return "service payed successfully";
  }

}