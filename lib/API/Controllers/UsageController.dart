import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:power/API/Models/UsageModel.dart';
import 'Controller.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';

class UsageController{


  static Future<List<UsageModel>> usage()async{
    var response =
    await http.get(Uri.parse('${Controller.url}/usage'), headers: {
      'Accept': 'application/json',
      'Authorization': 'Bearer ${GetStorage().read('token')}'
    });
    Map<String, dynamic> json = jsonDecode(response.body);

    List<UsageModel> models = [];

    for(var x in json['usage'])
      models.add(UsageModel.fromJSON(x));
    print(response.body);
    return models.reversed.toList();
  }


}