import 'package:get_storage/get_storage.dart';
import 'package:power/API/Models/WalletModel.dart';
import 'Controller.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';

class WalletController {

  static Future<List<WalletModel>> myWallet() async {
    var response =
        await http.get(Uri.parse('${Controller.url}/wallet/all'), headers: {
      'Accept': 'application/json',
      'Authorization': 'Bearer ${GetStorage().read('token')}'
    });
    Map<String, dynamic> json = jsonDecode(response.body);
    List<WalletModel> models = [];

    List wallet = json['wallet'];

    WalletModel.account = json['account'];

    for (var w in wallet)
      models.add(WalletModel.fromJSON(w));

    return models;
  }

  static Future<WalletModel> create(int amount) async {
    var response =
    await http.post(Uri.parse('${Controller.url}/wallet/create'), headers: {
      'Accept': 'application/json',
      'Authorization': 'Bearer ${GetStorage().read('token')}',
      'Content-Type': 'application/json'
    },
      body: jsonEncode({
        'amount':amount
      })
    );
    Map<String, dynamic> json = jsonDecode(response.body);
    var wallet = json['wallet'];
    return WalletModel.fromJSON(wallet);
  }
}
