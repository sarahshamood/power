import 'Controller.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';
import 'package:power/API/Models/CityModel.dart';

class CityController{


  static Future<List<CityModel>> getAllCities() async{
    var response = await http.get(Uri.parse('${Controller.url}/city/all'));
    Map<String,dynamic> json = jsonDecode(response.body);
    List<dynamic> citiesJson = json['cities'];
    List<CityModel> cities = [];

    for(var c in citiesJson)
      cities.add(CityModel.fromJSON(c));

    return cities;
  }



}