// ignore_for_file: unnecessary_string_interpolations, curly_braces_in_flow_control_structures

import 'package:get_storage/get_storage.dart';

import 'Controller.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';

class AuthController{

  static Future<String> register({
    required String email,
    required String name,
    required String password,
    required String phone,
    required int city_id,
  }) async {
    var response = await http.post(
        Uri.parse('${Controller.url}/register'),
        body: jsonEncode({
          'email':email,
          'name':name,
          'password':password,
          'phone':phone,
          'city_id':city_id,
        }),
      headers: {
        'Content-Type': 'application/json'
      }
    );
  Map<String,dynamic> json = jsonDecode(response.body);

  if(json['token'] != null){
    await GetStorage().write('token', json['token']);
    await GetStorage().write('city_id', json['user']['city_id']);
    return 'Register successfully , hello ${json['user']['name']}';
  }
    throw Exception(json['error']);
  }

  static Future<String> login({required String email, required String password,})async{
    var response = await http.post(
        Uri.parse('${Controller.url}/login'),
        body: jsonEncode({
          'email':email,
          'password':password,
        }),
        headers: {
          'Content-Type': 'application/json'
        }
    );
    Map<String,dynamic> json = jsonDecode(response.body);

    if(json['token'] != null){
      await GetStorage().write('token', json['token']);
      await GetStorage().write('city_id', json['user']['city_id']);
      return 'Login successfully , hello ${json['user']['name']}';
    }
    throw Exception(json['error']);
  }

  static Future<String> logout()async{
    var response = await http.delete(
        Uri.parse('${Controller.url}/logout'),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ${GetStorage().read('token')}'
        }
    );
    Map<String,dynamic> json = jsonDecode(response.body);

    await GetStorage().remove('token');
    await GetStorage().remove('city_id');

    return json['message'];
  }

}