
class UsageModel {
  UsageModel({
    required this.amount,
    required this.created_at,
  });

  final int amount;
  final DateTime created_at;

  factory UsageModel.fromJSON(Map<String, dynamic> json) => UsageModel(
    amount: json["amount"],
    created_at: DateTime.parse(json["created_at"]),
  );

  Map<String, dynamic> toJson() => {
    "amount": amount,
    "created_at": created_at.toIso8601String(),
  };
}