
class CityModel{
  final int id;
  final String name;
  final int users_count;

  CityModel({required this.id,required this.name,required this.users_count});

  factory CityModel.fromJSON(Map<String,dynamic> json)=>
      CityModel(
          id: json['id'],
          name: json['name'],
          users_count: json['users_count']
      );
}