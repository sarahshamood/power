
class WalletModel{

  final int amount;
  final DateTime created_at;
  static int account = 0;
  WalletModel({required this.amount,required this.created_at});

  factory WalletModel.fromJSON(Map<String,dynamic> json) =>
    WalletModel(
        amount: json['amount'],
        created_at: DateTime.parse(json['created_at'])
  );

}