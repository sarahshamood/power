
class ServiceModel {
  ServiceModel({
    required this.id,
    required this.value,
    required this.type,
    required this.watt_price
  });

  final int id;
  final int value;
  final int watt_price;
  final String type;

  factory ServiceModel.fromJSON(Map<String, dynamic> json) => ServiceModel(
    id: json["id"],
    value: json["value"],
    type: json['type'],
    watt_price: json['watt_price']
  );


}