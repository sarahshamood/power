
class UserServiceModel {
  UserServiceModel({
    required this.id,
    required this.sub_service_id,
    required this.used,
    required this.max,
    required this.created_at
  });

  final int id;
  final int sub_service_id;
  final num used;
  final int max;
  final DateTime created_at;

  factory UserServiceModel.fromJSON(Map<String, dynamic> json) => UserServiceModel(
    id: json["id"],
    sub_service_id: json["sub_service_id"],
    used: json["used"],
    max: json["max"],
    created_at: DateTime.parse(json['created_at']),
  );

  bool get isOver => used > max;

}