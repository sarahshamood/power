// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

class SelectValue extends StatefulWidget {

  final Function(int) onChange;
  final List<String> values;


  SelectValue({required this.onChange,this.values = const []});

  @override
  State<SelectValue> createState() => _SelectValueState();
}

class _SelectValueState extends State<SelectValue> {

  int? _selected;
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.all(size.width*0.025),
      padding: EdgeInsets.all(size.width*0.025),
      width: double.infinity,
      height: size.height*0.075,
      decoration: BoxDecoration(
        color: Colors.grey.withOpacity(0.25),
        borderRadius: BorderRadius.circular(size.width*0.025),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              Icon(Icons.location_on,color: Theme.of(context).disabledColor,),
              SizedBox(width: size.width*0.05,),
              Text(
                _selected == null ? 'city' : widget.values[_selected!],
                style: TextStyle(color: Theme.of(context).hintColor),
              ),
            ],
          ),
          IconButton(
            onPressed: (){
              showDialog(
                  context: context,
                  builder: (context) =>
                      Dialog(
                        child: ListView.builder(
                            itemCount: widget.values.length,
                            itemBuilder: (context,i) =>
                              ListTile(
                                title: Text(widget.values[i]),
                                leading: Icon(Icons.location_city),
                                onTap: (){
                                  setState(() {
                                    _selected = i;
                                    widget.onChange(i);
                                  });
                                  Navigator.pop(context);
                                },
                              ),
                        ),
                      ),
              );
            },
            icon: Icon(Icons.keyboard_arrow_down,color: Theme.of(context).disabledColor,),
          )
        ],
      ),
    );
  }
}