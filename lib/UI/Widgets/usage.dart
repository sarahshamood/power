// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';

class UsageView extends StatelessWidget {

  final int total;
  final int used;


  UsageView({required this.total,required  this.used});

  @override
  Widget build(BuildContext context) {
    late double ratio;
    if(total == used && used == 0)
     ratio = 0;
    else
      ratio= (used.toDouble()) / (total.toDouble());


    var size = MediaQuery.of(context).size;
    return Container(
      width: double.infinity,
      height: size.height*0.25,
      padding: EdgeInsets.all(size.width*0.025),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          SizedBox(
            width: double.infinity,
            height: size.height*0.15,
            child: Padding(
              padding: EdgeInsets.all(size.width*0.025),
              child: FittedBox(
                alignment: Alignment.centerLeft,
                child: Text('''
  Total  : $total
  Used  : $used
  Ratio  : ${((ratio*10000).toInt()).toDouble()/100}%''',
                  style: TextStyle(color: ((ratio < 1) ? Colors.indigo : Colors.red)[800]),
                ),
              ),
            ),
          ),
          Container(
            width: double.infinity,
            height: size.height*0.045,
            decoration: BoxDecoration(
              color: ((ratio < 1) ? Colors.indigo : Colors.red).withOpacity(0.5),
              borderRadius: BorderRadius.circular(size.height*0.02),
            ),
            child: Container(
              margin:(ratio < 1) ? EdgeInsets.only(right: size.width*(1.0 - ratio )) : EdgeInsets.zero,
              decoration: BoxDecoration(
                color:(ratio < 1) ? Colors.indigo : Colors.red,
                borderRadius: BorderRadius.circular(size.height*0.02),
              ),
            ),
          ),
        ],
      ),
      decoration: BoxDecoration(
          color: ((ratio < 1) ? Colors.indigo : Colors.red).withOpacity(0.2),
        boxShadow: [
          BoxShadow(
            offset: Offset(0,-5),
            color: ((ratio < 1) ? Colors.indigo : Colors.red).withOpacity(0.2),
            spreadRadius: 5,
            blurStyle: BlurStyle.outer,
            blurRadius: 25
          )
        ]
      ),
    );
  }
}
