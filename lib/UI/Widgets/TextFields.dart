import 'package:flutter/material.dart';

class MyTextField extends StatefulWidget {

  final String? hint;
  final Widget? icon;
  final bool obscureText;
  final TextEditingController controller;
  final TextInputType? keyboardType;
  bool _hide = true;
  MyTextField({required this.controller,this.hint, this.icon,this.obscureText = false,this.keyboardType});

  @override
  State<MyTextField> createState() => _MyTextFieldState();
}

class _MyTextFieldState extends State<MyTextField> {


  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Padding(
      padding: EdgeInsets.all(size.width*0.025),
      child: TextField(
        controller: widget.controller,
        decoration: InputDecoration(
          hintText: widget.hint,
          prefixIcon: widget.icon,
          suffixIcon: widget.obscureText
              ? IconButton(
              onPressed: ()=>setState((){
                widget._hide = ! widget._hide;
              }),
              icon: widget._hide ? Icon(Icons.visibility_off_outlined) : Icon(Icons.visibility_outlined)
          )
              :null,
          filled: true,
          fillColor: Colors.grey.withOpacity(0.25),
          border: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.circular(size.width*0.025),
          ),
        ),
        obscureText: widget.obscureText && widget._hide,
        keyboardType: widget.keyboardType,
      ),
    );
  }
}




