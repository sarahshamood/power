// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:power/API/Controllers/AuthController.dart';
import 'package:power/UI/Widgets/TextFields.dart';
import 'package:power/UI/Widgets/custom.dart';

class LoginScreen extends StatelessWidget {

  final TextEditingController emailController = TextEditingController();
  final TextEditingController passController = TextEditingController();

  @override
  Widget build(BuildContext context) {

    var size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: size.height*0.025,),
            Container(
              height: size.height*0.25,
              width: double.infinity,
              padding: EdgeInsets.symmetric(horizontal: size.width*0.025, vertical: size.width*0.05),
              child: Image.asset('images/logo.png'),
            ),
            SizedBox(height: size.height*0.08,),
            MyTextField(
              controller: emailController,
              icon: Icon(Icons.email),
              hint: 'email',
              keyboardType: TextInputType.emailAddress,
            ),
            MyTextField(
              controller: passController,
              icon: Icon(Icons.password),
              hint: 'password',
              obscureText: true,
            ),
            SizedBox(height: size.height*0.08,),
            ElevatedButton(
                onPressed: ()=> loginButton(context),
                child: Text('Login')
            ),
            Padding(
                padding: EdgeInsets.all(size.width*0.025),
                child: InkWell(
                  child: Text('if you don\'t have an account sign up.',
                  style: TextStyle(
                    color: Colors.deepPurpleAccent,
                  ),
                  ),
                  onTap: (){
                    Get.offNamed('/register');
                    },
                ),
            ),
            SizedBox(height: size.height*0.02,),
          ],
        ),
      ),
    );
  }

  void loginButton(context)async{
    if(emailController.text.isEmpty || passController.text.isEmpty ){
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Missing Data!')));
      return;
    }


    try{
      showDialog(
          context: context,
          builder: (context)=>WillPopScope(
              child: Loading(),
              onWillPop: () async{
                return false;
              },
          )
      );
      var res = await AuthController.login(
        email: emailController.text,
        password: passController.text,
    );
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(res)));
    Get.offAllNamed('/home');
    }
    catch(e){
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(e.toString().substring(11))));
      Navigator.pop(context);
    }

  }
}
