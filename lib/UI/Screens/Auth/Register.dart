// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, curly_braces_in_flow_control_structures

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:power/API/Controllers/AuthController.dart';
import 'package:power/API/Controllers/CityController.dart';
import 'package:power/API/Models/CityModel.dart';
import 'package:power/UI/Widgets/SelectValue.dart';
import 'package:power/UI/Widgets/TextFields.dart';
import 'package:power/UI/Widgets/custom.dart';

class RegisterScreen extends StatelessWidget {


  final TextEditingController emailController = TextEditingController();
  final TextEditingController nameController = TextEditingController();
  final TextEditingController phoneController = TextEditingController();
  final TextEditingController passController = TextEditingController();
  final TextEditingController cpassController = TextEditingController();

  int? _selected ;

  @override
  Widget build(BuildContext context) {

    var size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: Text('Register'),
      ),
      body: FutureBuilder<List<CityModel>>(
        future: CityController.getAllCities(),
        builder: (context,snapShot) {
          if(snapShot.hasData)
          return SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: size.height*0.025,),
                Container(
                  height: size.height*0.25,
                  width: double.infinity,
                  padding: EdgeInsets.symmetric(horizontal: size.width*0.025, vertical: size.width*0.05),
                  child: Image.asset('images/logo.png'),
                ),
                SizedBox(height: size.height*0.08,),

                MyTextField(
                  controller: emailController,
                  icon: Icon(Icons.email),
                  hint: 'email',
                  keyboardType: TextInputType.emailAddress,
                ),
                MyTextField(
                  controller: nameController,
                  icon: Icon(Icons.person),
                  hint: 'name',
                ),
                MyTextField(
                  controller: phoneController,
                  icon: Icon(Icons.phone),
                  hint: 'phone',
                  keyboardType: TextInputType.phone,
                ),
                SelectValue(
                  onChange: (val){
                    _selected = val;
                    print(val);
                  },
                  values: [
                    for(var c in snapShot.data!)
                      c.name
                  ],
                ),
                MyTextField(
                  controller: passController,
                  icon: Icon(Icons.password),
                  hint: 'password',
                  obscureText: true,
                ),
                MyTextField(
                  controller: cpassController,
                  icon: Icon(Icons.password),
                  hint: 'confirm password',
                  obscureText: true,
                ),
                SizedBox(height: size.height*0.08,),
                ElevatedButton(
                    onPressed: ()=>registerButton(snapShot.data![_selected??0].id,context),
                    child: Text('Register')
                ),
                Padding(
                  padding: EdgeInsets.all(size.width*0.025),
                  child: InkWell(
                    child: Text('already have an account? login.',
                      style: TextStyle(
                        color: Colors.deepPurpleAccent,
                      ),
                    ),
                    onTap: (){
                      Get.offNamed('/login');
                    },
                  ),
                ),
                SizedBox(height: size.height*0.02,),
              ],
            ),
          );
          return Center(
            child: snapShot.hasError ? Text(snapShot.error.toString()) : Loading(),
          );

        }
      ),
    );
  }

  void registerButton(int city_id,context)async{
    if(emailController.text.isEmpty || passController.text.isEmpty || phoneController.text.isEmpty
        || cpassController.text.isEmpty || nameController.text.isEmpty
    ){
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Missing Data!')));
      return;
    }
    else if(cpassController.text != passController.text){
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Confirm Password is wrong!')));
      return;
    }
    else if(_selected == null){
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('select City!')));
      return;
    }

    try{
      showDialog(
          context: context,
          builder: (context)=>WillPopScope(
            child: Loading(),
            onWillPop: () async{
              return false;
            },
          )
      );
      var res = await AuthController.register(
        email: emailController.text,
        name: nameController.text,
        password: passController.text,
        phone: phoneController.text,
        city_id: city_id
    );
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(res)));
    Get.offNamed('/home');
    }
    catch(e){
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(e.toString().substring(11))));
      Navigator.pop(context);
    }

  }
}
