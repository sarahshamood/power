// ignore_for_file: prefer_const_constructors, curly_braces_in_flow_control_structures, prefer_const_literals_to_create_immutables

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:power/API/Controllers/WalletController.dart';
import 'package:power/API/Models/WalletModel.dart';
import 'package:power/UI/Widgets/custom.dart';


class WalletScreen extends StatelessWidget {

  bool _bottom = false;
  final TextEditingController amountController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text('My Wallet'),
      ),
      body: FutureBuilder<List<WalletModel>>(
        future: WalletController.myWallet(),
        builder: (context,snapShot){
          if(snapShot.hasData)
            return Column(
              children: [
                Container(
                  padding: EdgeInsets.all(size.width*0.025),
                  width: double.infinity,
                  height: size.height*0.15,
                  color: Colors.indigo.withOpacity(0.5),
                  child: FittedBox(
                    child: DefaultTextStyle(
                      style: TextStyle(
                        color: Colors.white
                      ),
                      child: Row(
                        children: [
                          Icon(Icons.credit_card,color: Colors.white,),
                          SizedBox(width: size.width*0.025,),
                          Text('total in account : ${WalletModel.account.toString()}'),
                          SizedBox(width: size.width*0.025,),
                        ],
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: ListView.builder(
                      itemCount: snapShot.data!.length,
                      itemBuilder: (context,i) =>
                          Card(
                            child: ListTile(
                              leading: Icon(Icons.credit_card),
                              title: Text('added at  ${snapShot.data![i].created_at.year} / ${snapShot.data![i].created_at.month} / ${snapShot.data![i].created_at.day}'),
                              subtitle: Text('amount : ${snapShot.data![i].amount}'),
                            ),
                          ),
                  ),
                )
              ],
            );

          return Center(
            child: snapShot.hasError ? Text(snapShot.error.toString()) : Loading(),
          );

        },
      ),
      floatingActionButton: StatefulBuilder(
        builder: (context,setState) {
          return FloatingActionButton(
            onPressed: (){


            if(_bottom)
                Navigator.pop(context);
              else
                showBottomSheet(
                  context: context,

                  builder: (context)=>
                      BottomSheet(
                          enableDrag: false,
                          onClosing: (){
                            _bottom = false;
                          },
                          builder: (context) =>WillPopScope(
                            onWillPop: () async{
                              return false;
                            },
                            child: GestureDetector(
                              child: Container(
                                height: size.height*0.60,
                                width: double.infinity,
                                color: Colors.indigo.withOpacity(0.2),
                                padding: EdgeInsets.symmetric(horizontal: size.width*0.025).copyWith(top: size.height*0.05),
                                child: TextField(
                                  autofocus: true,
                                  keyboardType: TextInputType.number,
                                  controller: amountController,

                                  decoration: InputDecoration(
                                      hintText: 'amount',
                                      prefixIcon: Icon(Icons.credit_card),
                                      suffixIcon: IconButton(
                                        onPressed: ()async{

                                          try{
                                            int amount = int.parse(amountController.text);
                                            showDialog(
                                                context: context,
                                                builder: (context)=>WillPopScope(
                                                  child: Loading(),
                                                  onWillPop: () async{
                                                    return false;
                                                  },
                                                )
                                            );
                                            await WalletController.create(amount);
                                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('done')));
                                          }
                                          catch(e){
                                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('pleas enter correct value')));
                                          }
                                          amountController.text = '';
                                          Navigator.pop(context);
                                          Get.offNamed('/wallet');
                                        },
                                        icon: Icon(Icons.check),
                                      )
                                  ),
                                ),
                              ),
                              onVerticalDragDown: (_){},
                            ),
                          ),
                      ),

              );

              setState((){
                amountController.text = '';
                _bottom =! _bottom;
              });
            },
            elevation: 0,
            child: Icon(_bottom ?Icons.close :  Icons.add),
          );
        }
      ),
    );
  }
}
