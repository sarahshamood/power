// ignore_for_file: curly_braces_in_flow_control_structures

import 'package:flutter/material.dart';
import 'package:power/API/Controllers/UsageController.dart';
import 'package:power/API/Models/UsageModel.dart';
import 'package:power/UI/Widgets/custom.dart';

class UsageScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('My Usage'),
      ),
      body: FutureBuilder<List<UsageModel>>(
        future: UsageController.usage(),
        builder: (context,snapShot){
          if(snapShot.hasData)
            return ListView.builder(
              itemCount: snapShot.data!.length,
              itemBuilder: (context,i) =>
                  Card(
                    child: ListTile(
                      leading: Icon(Icons.data_usage_outlined),
                      title: Text('added at  ${snapShot.data![i].created_at.year} / ${snapShot.data![i].created_at.month} / ${snapShot.data![i].created_at.day}'),
                      subtitle: Text('amount : ${snapShot.data![i].amount}'),
                    ),
                  ),
            );

          return Center(
            child: snapShot.hasError ? Text(snapShot.error.toString()) : Loading(),
          );

        },
      ),

    );
  }
}