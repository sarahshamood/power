// ignore_for_file: use_key_in_widget_constructors, prefer_const_constructors, curly_braces_in_flow_control_structures

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:power/API/Controllers/UserServiceController.dart';
import 'package:power/API/Models/UserServiceModel.dart';
import 'package:power/UI/Screens/Services/ServiceInfo.dart';
import 'package:power/UI/Screens/Services/UsageInfo.dart';
import 'package:power/UI/Widgets/custom.dart';

class PaymentsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text('My Payments'),
      ),
      body: FutureBuilder<List<UserServiceModel>>(
        future: UserServiceController.myServices(),
        builder: (context,snapShot){
          if(snapShot.hasData)
            return ListView.builder(
              itemCount: snapShot.data!.length,
              itemBuilder: (context,i) =>
                  Card(
                    child: ListTile(
                      leading: Icon(Icons.data_usage_outlined,color: snapShot.data![i].isOver ? Colors.red : Colors.blue ,),
                      subtitle: Text('added at  ${snapShot.data![i].created_at.year} / ${snapShot.data![i].created_at.month} / ${snapShot.data![i].created_at.day}'),
                      title: Text('max : ${snapShot.data![i].max} - used : ${snapShot.data![i].used}'),
                      trailing: IconButton(
                        onPressed: (){
                          Get.to(UsageInfoScreen(snapShot.data![i]));
                        },
                        icon: CircleAvatar(
                          backgroundColor: snapShot.data![i].isOver ? Colors.red : Colors.blue ,
                          child: Icon(Icons.arrow_forward,color: Colors.white,),
                        ),
                      ),
                    ),
                  ),
            );

          return Center(
            child: snapShot.hasError ? Text(snapShot.error.toString()) : Loading(),
          );

        },
      ),
      floatingActionButton: ElevatedButton(
        onPressed: ()async{
          showDialog(
              context: context,
              builder: (context)=>WillPopScope(
                child: Loading(),
                onWillPop: () async{
                  return false;
                },
              )
          );
          UserServiceController.payAllServices();
          Get.offAllNamed('/home');
        },
        child: Text('Pay All'),
        style: ElevatedButton.styleFrom(shape: RoundedRectangleBorder(
            borderRadius:  BorderRadius.circular(size.width*0.025)
        ),
        primary: Colors.blue
        ),
      ),
    );
  }
}
