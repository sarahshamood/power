// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, curly_braces_in_flow_control_structures

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:power/API/Controllers/AuthController.dart';
import 'package:power/API/Controllers/UserServiceController.dart';
import 'package:power/API/Models/ServiceModel.dart';
import 'package:power/API/Models/UserServiceModel.dart';
import 'package:power/UI/Screens/Services/ServiceInfo.dart';
import 'package:power/UI/Widgets/custom.dart';
import 'package:power/UI/Widgets/usage.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    StateSetter? setS;
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
        actions: [
          IconButton(
            onPressed: (){
              if(setS != null)
                setS!((){});
            },
            icon: Icon(Icons.replay)
          )
        ],
      ),
      body: Column(
        children: [
          StatefulBuilder(
            builder: (context,setState) {
              setS = setState;
              return SizedBox(
                height: size.height*0.25,
                child: FutureBuilder<List<UserServiceModel>>(
                  future: UserServiceController.myServices(),
                  builder: (context,snapShot){
                    if(snapShot.hasData)
                      if(snapShot.data!.isEmpty)
                        return UsageView(total: 0, used: 0);
                      else
                      return PageView.builder(
                          itemCount: snapShot.data!.length,
                          itemBuilder: (context,i)=>UsageView(
                              total: snapShot.data![i].max,
                              used: snapShot.data![i].used.toInt()
                          )
                      );

                    return Center(
                      child: snapShot.hasError ? Text(snapShot.error.toString()) : Loading(),
                    );
                  },
                ),
              );
            }
          ),
          Expanded(
            child: FutureBuilder<List<ServiceModel>>(
              future: UserServiceController.allServices(),
              builder: (context,snapShot){
                if(snapShot.hasData)
                  return ListView.builder(
                      itemCount: snapShot.data!.length,
                      itemBuilder: (context,i) =>
                          Card(
                            child: ListTile(
                              leading: CircleAvatar(
                                child: Icon(Icons.power),
                              ),
                              title: Text('Service Type : ${snapShot.data![i].type}'),
                              subtitle: Text('watt value : ${snapShot.data![i].value}'),
                              trailing: IconButton(
                                onPressed: (){
                                  Get.to(ServiceInfoScreen(snapShot.data![i]));
                                },
                                icon: CircleAvatar(
                                  child: Icon(Icons.arrow_forward),
                                ),
                              ),
                            ),
                          )
                  );

                return Center(
                  child: snapShot.hasError ? Text(snapShot.error.toString()) : Loading(),
                );
              },
            )
          )
        ],
      ),
      drawer: Drawer(
        child: ListView(
          children: [
            DrawerHeader(
              padding: EdgeInsets.zero,
                child: Container(
                  padding: EdgeInsets.all(size.width*0.025),
                  color: Colors.indigo[500],
                  child: FittedBox(
                    child: Row(
                      children: [
                        Icon(Icons.power,color: Colors.white,),
                        Text('POWER ',style: TextStyle(color: Colors.white),),
                      ],
                    ),
                  ),
                )
            ),
            ListTile(
              title: Text('Wallet'),
              leading: Icon(Icons.credit_card),
              onTap: (){
                Get.toNamed('/wallet');
              },
            ),
            ListTile(
              title: Text('Usage history'),
              leading: Icon(Icons.data_usage),
              onTap: (){
                Get.toNamed('/usage');
              },
            ),
            ListTile(
              title: Text('Payments'),
              leading: Icon(Icons.payments_outlined),
              onTap: (){
                Get.toNamed('/payments');
              },
            ),
            Divider(thickness: 1.5,),
            ListTile(
              title: Text('Logout'),
              leading: Icon(Icons.logout),
              onTap: ()async{
                showDialog(
                    context: context,
                    builder: (context)=>Loading()
                );
                await AuthController.logout();
                Get.offAllNamed('/login');
              },
            ),

          ],
        ),
      ),
    );
  }
}
