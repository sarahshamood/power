// ignore_for_file: use_key_in_widget_constructors, curly_braces_in_flow_control_structures

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:power/API/Controllers/UserServiceController.dart';
import 'package:power/API/Models/ServiceModel.dart';
import 'package:power/API/Models/UserServiceModel.dart';
import 'package:power/UI/Widgets/custom.dart';
import 'package:power/UI/Widgets/usage.dart';


class UsageInfoScreen extends StatelessWidget {

  final UserServiceModel model;


  UsageInfoScreen(this.model);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text('Usage'),
      ),
      body: FutureBuilder<ServiceModel>(
        future: UserServiceController.showService(model.sub_service_id),
        builder: (context,snapShot){
          if(snapShot.hasData)
            return ListView(
              children: [
                UsageView(
                    total: model.max,
                    used: model.used.toInt()
                ),

                ListTile(
                  leading: Icon(Icons.electrical_services),
                  title: Text('type : ${snapShot.data!.type}'),
                ),
                ListTile(
                  leading: Icon(Icons.monetization_on_outlined),
                  title: Text('Watt price : ${snapShot.data!.watt_price} s.p'),
                ),
                ListTile(
                  leading: Icon(Icons.power_sharp),
                  title: Text('Max watt amount : ${snapShot.data!.value} Watt'),
                ),
                ListTile(
                  leading: Icon(Icons.power_sharp,color:model.isOver ? Colors.red :Colors.indigo),
                  title: Text('Used watt amount : ${model.used} Watt',style: TextStyle(
                    color:model.isOver ? Colors.red :Colors.indigo
                  ),),

                ),
                ListTile(
                  leading: Icon(Icons.monetization_on),
                  title: Text('Total price : ${model.used * snapShot.data!.watt_price} s.p'),
                ),

                Padding(
                  padding:EdgeInsets.symmetric(horizontal: size.width*0.35,vertical: size.height*0.075),
                  child: ElevatedButton.icon(
                      onPressed: ()async{
                        showDialog(
                            context: context,
                            builder: (context)=>WillPopScope(
                              child: Loading(),
                              onWillPop: () async{
                                return false;
                              },
                            )
                        );
                        await UserServiceController.payService(model.id);
                        Get.offAllNamed('/home');
                      },
                      icon: Icon(Icons.payments_outlined),
                      label: Text(' Pay')
                  ),
                )
              ],
            );


          return Center(
            child: snapShot.hasError ? Text(snapShot.error.toString()) : Loading(),
          );
        },
      )

    );
  }
}
