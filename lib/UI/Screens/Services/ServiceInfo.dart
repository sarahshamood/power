// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:power/API/Controllers/UserServiceController.dart';
import 'package:power/API/Models/ServiceModel.dart';
import 'package:power/UI/Widgets/custom.dart';

class ServiceInfoScreen extends StatelessWidget {

  final ServiceModel model;


  ServiceInfoScreen(this.model);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text('Service information'),
      ),
      body: ListView(
        children: [
          Image.asset('images/tower.png',
            fit: BoxFit.cover,
          ),
          ListTile(
            leading: Icon(Icons.electrical_services),
            title: Text('type : ${model.type}'),
          ),
          ListTile(
            leading: Icon(Icons.monetization_on_outlined),
            title: Text('Watt price : ${model.watt_price} s.p'),
          ),
          ListTile(
            leading: Icon(Icons.power_sharp),
            title: Text('Watt amount : ${model.value} Watt'),
          ),
          ListTile(
            leading: Icon(Icons.monetization_on),
            title: Text('Total price : ${model.value * model.watt_price} s.p'),

          ),
          Padding(
            padding:EdgeInsets.symmetric(horizontal: size.width*0.35,vertical: size.height*0.075),
            child: ElevatedButton.icon(
                onPressed: ()async{
                  showDialog(
                      context: context,
                      builder: (context)=>WillPopScope(
                        child: Loading(),
                        onWillPop: () async{
                          return false;
                        },
                      )
                  );
                  await UserServiceController.byService(model.id);
                  Get.offAllNamed('/home');
                },
                icon: Icon(Icons.shopping_cart),
                label: Text('Sale')
            ),
          )
        ],
      ),
    );
  }
}
