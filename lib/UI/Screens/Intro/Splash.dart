// ignore_for_file: prefer_const_constructors, curly_braces_in_flow_control_structures

import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:power/UI/Screens/Auth/Login.dart';
import 'package:power/UI/Screens/User/Home.dart';


class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: Future.delayed(Duration(seconds: 1,milliseconds: 200)).then((value) => true),
        builder: (context,snapShot){
          if(snapShot.hasData)
            if(GetStorage().hasData('token'))
            return HomeScreen();
            else
              return LoginScreen();

          return Scaffold(
          body: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [Colors.indigo.withOpacity(0.2),Colors.white],
                begin: Alignment.bottomRight,
                end: Alignment.topLeft
              )
            ),
            child: Center(
              child:Padding(
                padding: const EdgeInsets.all(16.0),
                child: Image.asset('images/logo.png'),
              ),
            ),
          ),  
          );
          
        },
      );
  }
}
