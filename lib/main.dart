// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:power/UI/Screens/Intro/Splash.dart';
import 'UI/Screens/Auth/Login.dart';
import 'UI/Screens/Auth/Register.dart';
import 'UI/Screens/User/Home.dart';
import 'UI/Screens/User/Payments.dart';
import 'UI/Screens/User/Usage.dart';
import 'UI/Screens/User/Wallet.dart';

void main() async {
  await GetStorage.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData.light().copyWith(
        appBarTheme: AppBarTheme(
          color: Color(0xFF656565),
          elevation: 0,
          centerTitle: true
        ),
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(
            elevation: 0,
            shape: RoundedRectangleBorder(),
            primary: Colors.indigoAccent,
          )
        ),

      ),


      getPages: [
        GetPage(name: '/login', page: ()=>LoginScreen()),
        GetPage(name: '/register', page: ()=>RegisterScreen()),
        GetPage(name: '/home', page: ()=>HomeScreen()),
        GetPage(name: '/wallet', page: ()=>WalletScreen()),
        GetPage(name: '/usage', page: ()=>UsageScreen()),
        GetPage(name: '/payments', page: ()=>PaymentsScreen()),
      ],

      home: SplashScreen(),
    );
  }
}

